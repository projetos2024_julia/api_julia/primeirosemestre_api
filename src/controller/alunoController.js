module.exports = class alunoController{


    static async postAluno(req, res) {
        const { nome, idade, profissao, cursoMatriculado } = req.body;
        console.log('Dados recebidos:', { nome, idade, profissao, cursoMatriculado });
        res.status(200).json({ message:'Dados Recebidos!'});
      }
    
      static async updateAluno(req, res) {
        const { nome, idade, profissao, cursoMatriculado } = req.body;
        if (!nome || !idade || !profissao || !cursoMatriculado) {
          return res.status(400).json({ message: "Todos os campos são obrigatórios." });
      }
      const aluno = {
          nome: nome,
          idade: idade,
          profissao: profissao,
          cursoMatriculado: cursoMatriculado,
      };
        console.log("Aluno editado:", aluno);
        res.status(200).json({ message: "Aluno editado:", aluno: aluno });
      }
    
      
      static async deleteAluno(req, res) {
        const alunoId = req.params.id;
        return res.status(200).json({ message: 'Aluno removido com sucesso.', alunoId: alunoId });
        
      }
    

}