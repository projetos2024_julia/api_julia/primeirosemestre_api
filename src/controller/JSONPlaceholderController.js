const axios = require("axios");

module.exports = class JSONPlaceholderController {
    static async getUsers(req, res) {
        try {
            const response = await axios.get("https://jsonplaceholder.typicode.com/users");
            const users = response.data;

            res.status(200).json({ messagem: 'Aqui estão os usuários captados da API pública JSONPlaceholder:', users });
        } catch (error) {
            console.error(error);
            res.status(500).json({ error: "Falha ao encontrar usuários" });
        }
    }

    static async getWebsiteIO(req, res) {
        try {
            const response = await axios.get("https://jsonplaceholder.typicode.com/users");
            const users = response.data.filter(
                (user) => user.website.endsWith(".io")
            )
            res.status(200).json({ messagem: 'Aqui estão os usuários com domínio IO', users, });

        } catch (error) {
            console.error(error);
            res.status(500).json({ error: "Falha ao encontrar usuários" });
        }
    };

    static async getUsersWebsiteCOM(req, res) {
        try {
            const response = await axios.get("https://jsonplaceholder.typicode.com/users");
            const users = response.data.filter(
                (user) => user.website.endsWith(".com")
            )
            res.status(200).json({ messagem: 'Aqui estão os usuários com domínio COM', users, });

        } catch (error) {
            console.error(error);
            res.status(500).json({ error: "Falha ao encontrar usuários" });
        }
    };


    static async getUsersWebsiteNET(req, res) {
        try {
            const response = await axios.get("https://jsonplaceholder.typicode.com/users");
            const users = response.data.filter(
                (user) => user.website.endsWith(".net")
            )
            res.status(200).json({ messagem: 'Aqui estão os usuários com domínio NET', users, });

        } catch (error) {
            console.error(error);
            res.status(500).json({ error: "Falha ao encontrar usuários" });
        }
    };

    static async getCountDomain(req, res) {
        const { dominio } = req.query;

        if (!dominio) {
            return res.status(400).json({ error: "Domínio não fornecido" });
        }

        try {
            const response = await axios.get("https://jsonplaceholder.typicode.com/users");

            const users = response.data.filter(
                (user) => user.website.endsWith("." + dominio)
            );

            const count = users.length;

            res.status(200).json({
                message: `Aqui estão os usuários com domínio .${dominio}: ${count}!`,
            });
        } catch (error) {
            console.error(error);
            res.status(500).json({
                error: `Não foi possível encontrar os usuários com domínio .${dominio}`,
            });
        }
    }

};