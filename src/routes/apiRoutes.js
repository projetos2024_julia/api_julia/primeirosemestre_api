const router = require('express').Router()
const teacherController = require('../controller/teacherController')
const alunoController = require("../controller/alunoController")
const JSONPlaceholderController = require('../controller/JSONPlaceholderController')

//router.get('/ALGUMA-COISA/', algumaController.getAlgumaCoisa)
router.get('/docente/', teacherController.getTeacher);
router.post('/cadastroaluno/', alunoController.postAluno);
router.put('/atualizacaoAluno/', alunoController.updateAluno);
router.delete('/deleteAluno/:id', alunoController.deleteAluno);

router.get("/external/", JSONPlaceholderController.getUsers);
router.get("/external/io", JSONPlaceholderController.getWebsiteIO);
router.get("/external/com", JSONPlaceholderController.getUsersWebsiteCOM);
router.get("/external/net", JSONPlaceholderController.getUsersWebsiteNET);

router.get('/external/filter', JSONPlaceholderController.getCountDomain);




module.exports = router